#include <ezButton.h>
#include <Servo.h>

Servo servo;

int startProfile = 4;
int endProfile = 9;

int lastAngle = 0;
bool active = false;

void open(int angle, int motorSpeed = 20)
{
  bool dir = angle > lastAngle ? true : false;
  int newAngle = lastAngle + (dir ? 1 : -1);
  int diff = abs(angle - newAngle);
  servo.write(newAngle);
  lastAngle = newAngle;
  delay(motorSpeed);
  if (diff > 1) { open(angle, motorSpeed); }
}

void close(int motorSpeed = 20)
{ open(0, motorSpeed); }

void execute(int currentProfile = startProfile)
{
  if (currentProfile == startProfile)
  {
    open(100);
    delay(3000);
    close(5);
  }
  switch (currentProfile)
  {
    case 4:
    open(40, 80);
    delay(1000);
    close(120);
    break;

    case 5:
    open(41);
    delay(200);
    close();
    delay(300);
    open(52);
    delay(200);
    close();
    delay(100);
    open(39);
    delay(200);
    close();
    open(33);
    delay(200);
    close();
    open(30);
    delay(200);
    close();
    open(28);
    delay(200);
    close();
    break;

    case 6:
    open(30);
    open(10, 100);
    close();
    break;

    case 7:
    open(30);
    delay(100);
    close();
    open(30);
    delay(100);
    close();
    open(35);
    delay(100);
    close();
    open(42);
    delay(100);
    close(80);
    break;

    case 8:
    open(50);
    open(25);
    open(48);
    open(20);
    open(45);
    open(15);
    open(40);
    open(10);
    open(35);
    open(5);
    delay(1000);
    open(42);
    open(5);
    open(35);
    open(20);
    close(200);
    break;

    case 9:
    open(28);
    delay(5000);
    close(65);
    break;
  }
  if (currentProfile == endProfile)
  {
    open(100);
    delay(1000);
    close();
    delay(1000);
    open(100);
    delay(1000);
    close();
    digitalWrite(4, HIGH);
    digitalWrite(5, LOW);
    active = false;
  }
  else
  { 
    delay(5000);
    execute(currentProfile + 1); 
  }
}

void setup()
{
  Serial.begin(9600);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  servo.attach(2);
  servo.write(0);
  digitalWrite(4, HIGH);
  digitalWrite(5, LOW);
}

void loop()
{
  if (digitalRead(3) == LOW && !active)
  {
    active == true;
    digitalWrite(4, LOW);
    digitalWrite(5, HIGH);
    execute();
  }
}
